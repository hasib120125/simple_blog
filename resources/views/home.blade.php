@extends('index')

@section('content')

<div class="news">
                    <div class="news-grids">
                        <div class="col-md-8 news-grid-left">
                            <h3>latest news & events</h3>
                            <ul>
                                <li>
                                    <div class="news-grid-left1">
                                        <img src="images/16.jpg" alt=" " class="img-responsive" />
                                    </div>
                                    <div class="news-grid-right1">
                                        <h4><a href="single.html">Mexico's oil giant is in uncharted waters</a></h4>
                                        <h5>By <a href="#">Elizibeth Malkin</a> <label>|</label> <i>30.03.2016</i></h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                    <div class="clearfix"> </div>
                                </li>
                                <li>
                                    <div class="news-grid-left1">
                                        <img src="images/17.jpg" alt=" " class="img-responsive" />
                                    </div>
                                    <div class="news-grid-right1">
                                        <h4><a href="single.html">second wave of votes to legalize marijuana</a></h4>
                                        <h5>By <a href="#">james smith</a> <label>|</label> <i>30.03.2016</i></h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                    <div class="clearfix"> </div>
                                </li>
                                <li>
                                    <div class="news-grid-left1">
                                        <img src="images/13.jpg" alt=" " class="img-responsive" />
                                    </div>
                                    <div class="news-grid-right1">
                                        <h4><a href="single.html">Antares rocket, bound for space station, explodes</a></h4>
                                        <h5>By <a href="#">Michael Drew</a> <label>|</label> <i>30.03.2016</i></h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                    <div class="clearfix"> </div>
                                </li>
                                <li>
                                    <div class="news-grid-left1">
                                        <img src="images/12.jpg" alt=" " class="img-responsive" />
                                    </div>
                                    <div class="news-grid-right1">
                                        <h4><a href="single.html">stronger family bonds, two years after hurricane sandy</a></h4>
                                        <h5>By <a href="#">james smith</a> <label>|</label> <i>30.03.2016</i></h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                    <div class="clearfix"> </div>
                                </li>
                                <li>
                                    <div class="news-grid-left1">
                                        <img src="images/15.jpg" alt=" " class="img-responsive" />
                                    </div>
                                    <div class="news-grid-right1">
                                        <h4><a href="single.html">royal crush giants and force game 7</a></h4>
                                        <h5>By <a href="#">Michael Drew</a> <label>|</label> <i>30.03.2016</i></h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                    <div class="clearfix"> </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-4 news-grid-right">
                            <div class="news-grid-rght1">
                            <!-- Nav tabs -->
                              <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a class="high" href="#home" aria-controls="home" role="tab" data-toggle="tab">weather in London</a></li>
                                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">edit location</a></li>
                              </ul>

                              <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active london" id="home">
                                        <ul>
                                            <li>
                                                <h4>Wednesday</h4>
                                                <span></span>
                                                <p>21<sup>°</sup></p>
                                            </li>
                                            <li>
                                                <h4>Thursday</h4>
                                                <span class="moon"></span>
                                                <p>25<sup>°</sup></p>
                                            </li>
                                            <li>
                                                <h4>Friday</h4>
                                                <span class="sun"></span>
                                                <p>31<sup>°</sup></p>
                                            </li>
                                            <div class="clearfix"> </div>
                                        </ul>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="profile">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26359652.109742895!2d-113.72446020222534!3d36.24602872499641!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited+States!5e0!3m2!1sen!2sin!4v1450786850582" frameborder="0" style="border:0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="news-grid-rght2">
                                <h3>subscribe to our newsletter</h3>
                                <p>Get the latest news and updates by signing up to our daily newsletter.We won't sell your email or spam you !</p>
                                <form>
                                    <input type="text" value="enter your Email address" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'enter your Email address';}">
                                    <input type="submit" value="Submit">
                                </form>
                            </div>
                            <div class="news-grid-rght3">
                                <img src="images/18.jpg" alt=" " class="img-responsive" />
                                <div class="story">
                                    <p>story of the week</p>
                                    <h3><a href="single.html">US hails west Africa Ebola progress</a></h3>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>

@endsection